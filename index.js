'use strict';

const apicall = require('@rhc/apicalls');

export function getRequest(url){
	return apicall.getAPIcall(url);
};

export function postRequest(url,data){
	return apicall.postAPIcall(url,data);
};

export function deleteRequest(url){
	return apicall.deleteAPIcall(url);
};